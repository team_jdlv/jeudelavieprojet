package jeuDeLaVie.ihm;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import javax.swing.*;

import jeuDeLaVie.model.Plateau;

/**
 * @author fadwa
 * 
 *         Cette classe impl�mente le tableau du jeu
 *
 */
public class MyGrid extends JPanel
implements MouseWheelListener, MouseListener {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static boolean isMouseListener;
    /**
     * nombre de lignes de la grille
     * 
     * @see MyGrid#MyGrid(int, int, int)
     * @see MyGrid#getRows()
     * @see MyGrid#setRows(int)
     * @see MyGrid#paintComponent(Graphics)
     */
    private int rows;
    /**
     * nombre de colonnes de la grille
     * 
     * @see MyGrid#MyGrid(int, int, int)
     * @see MyGrid#getCols()
     * @see MyGrid#setCols(int)
     */
    private int cols;
    /**
     * taille de la grille
     * 
     * @see MyGrid#MyGrid(int, int, int)
     * @see MyGrid#getTaille()
     * @see MyGrid#setTaille(int)
     */
    private int taille;
    /**
     * instance du plateau (noyeu fonctionnel)
     * 
     * @see MyGrid#getPlateau()
     * @see MyGrid#setPlateau(Plateau)
     */
    private Plateau plateau;
    /**
     * la couleur des bors du rectangle de la grille
     * 
     * @see MyGrid#getColor()
     * @see MyGrid#setColor(Color)
     */
    private static Color color;

    /**
     * Constructeur de la grille
     * 
     * @param row
     *            nombre de lignes
     * @param col
     *            nombre des colonnes
     * @param taille
     *            la taille du tableau
     */
    public MyGrid(int row, int col, int taille) {
        super();
        this.rows = row;
        this.cols = col;
        this.taille = taille;
        this.addMouseWheelListener(this);
        plateau = new Plateau(1000);
        // repaint();
    }

    /**
     * Retourne le nombre de ligne de la grille
     * 
     * @return le nombre de lignes
     */
    public int getRows() {
        return rows;
    }

    /**
     * Met � jour les lignes
     * 
     * @param row
     *            le nouveau nombre de lignes
     */
    public void setRows(int row) {
        this.rows = row;
    }

    /**
     * Retourne le nombre de colonnes de la grille
     * 
     * @return le nombre de lignes
     */
    public int getCols() {
        return cols;
    }

    /**
     * Met � jour les colonnes
     * 
     * @param col
     *            le nouveau nombre de colonnes
     */
    public void setCols(int col) {
        this.cols = col;
    }

    /**
     * Retourne la taille de la grille
     * 
     * @return la taille
     */
    public int getTaille() {
        return taille;
    }

    /**
     * Met � jour la taille de la grille
     * 
     * @param taille
     *            la nouvelle taille
     */
    public void setTaille(int taille) {
        this.taille = taille;
    }

    /**
     * Retourne le plateau du jeu
     * 
     * @return une instance du plateau
     */
    public Plateau getPlateau() {
        return plateau;
    }

    /**
     * Met � jour le plateau
     * 
     * @param plateau
     *            nouveau plateau
     */
    public void setPlateau(Plateau plateau) {
        this.plateau = plateau;
    }

    /**
     * Retourne la couleur utilis�e
     * 
     * @return la couleur
     */
    public Color getColor() {
        return color;
    }

    /**
     * Met � jour la couleur des bords du rectangle
     * 
     * @param couleur
     *            la nouvelle couleur
     */
    public static void setColor(Color couleur) {
        color = couleur;
    }

    public void paintComponent(Graphics graphic) {
        super.paintComponents(graphic);
        // boucle pour dessiner le plateau du jeu
        for (int i = 0; i < (taille * rows); i = i + taille) {
            for (int j = 0; j < (taille * cols); j = j + taille) {
                /*
                 * si l'etat de la cellule est true on dessine un rectangle
                 * colori� en noir
                 */
                if (plateau.getTableau(i, j).getEtat()) {
                    graphic.setColor(Color.black);
                    graphic.fillRect(i, j, taille, taille);
                } else { /*
                 * sinon on dessine le rectangle avec les bords d'une
                 * couleur � partir du mode du jeu
                 */
                    graphic.setColor(getColor());
                    graphic.drawRect(i, j, taille, taille);
                }
            }
        }
    }

    public void mouseWheelMoved(MouseWheelEvent m) {
        // si on fait un Zoom in
        if (m.getWheelRotation() < 0) {
            taille--;
        } else if (m.getWheelRotation() > 0) { // sinon zoom out
            taille++;
        }
        this.repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        /* si en est en mode pause et on clique sur une case blanche 
         * elle devient noir et vice versa
         */
        if (getIsMouseListener()) {
            Graphics graphic = getGraphics();
            try {
                plateau.setCellule(e.getX(), e.getY());
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            if (graphic.getColor() == Color.white) {
                graphic.setColor(Color.black);
                graphic.fillRect(e.getX(), e.getY(), taille, taille);
            } else {
                graphic.setColor(Color.black);
                graphic.drawRect(e.getX(), e.getY(), taille, taille);
            }
            // repaint();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    /**
     * Retourne l'etat d'activation du mousewheelListener
     * 
     * @return l'etat en boolean
     */
    public boolean getIsMouseListener() {
        return isMouseListener;
    }

    /**
     * Met � jour l'�tat d'activation du mousewheelListener pour activer ou
     * d�sactiver le clique sur le plateau
     * 
     * @param etat
     *            le nouvel �tat d'activation
     */
    public static void setIsMouseListener(boolean etat) {
        isMouseListener = etat;
    }

}
