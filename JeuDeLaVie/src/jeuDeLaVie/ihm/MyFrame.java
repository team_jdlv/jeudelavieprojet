package jeuDeLaVie.ihm;

import java.awt.Dimension;

import javax.swing.*;

/**
 * @author fadwa
 *
 *
 *         Cette classe "MyFrame" gen�re l'interface principale du jeu.
 *
 */
public class MyFrame extends JFrame {

    private static final long serialVersionUID = 1L;

    /**
     * Panel de MyFrame
     */
    private JPanel panel;

    /**
     * Constructeur de MyFrame
     * 
     * @param title
     *            = titre de la frame
     * 
     * @see MyPanel#MyPanel()
     * 
     */
    public MyFrame(String title) {
        panel = new MyPanel();
        this.setTitle(title);
        this.setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        this.add(Box.createRigidArea(new Dimension(0, 50)));
        this.add(panel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
    }

    /**
     * La m�thode makeIt() Cr�e l'interface graphique et l'affiche
     */
    private static void makeIt() {
        // cr�e et configure la fen�tre
        JFrame frame = new MyFrame("Jeu de la Vie");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // affiche la fen�tre
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(final String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                makeIt();
            }
        });
    }
}
