package jeuDeLaVie.ihm;

import java.awt.*;

import javax.swing.*;

/**
 * @author fadwa
 * 
 *         conteneur des composant des param�tres du jeu
 *
 */
public class MyBoxSetting extends Box {

    /**
     * 
     */
    private static final long serialVersionUID = -982516791046241560L;
    /**
     * conteneur des de la combobox de la mortSolitude
     * 
     * @see MyBoxSetting#mortSCbox
     * 
     */
    private Box mSBox;
    /**
     * conteneur des de la combobox de la mortAsphyxie
     * 
     * @see MyBoxSetting#mortACbox
     * 
     */
    private Box mABox;
    /**
     * conteneur des de la combobox de la vieMin
     * 
     * @see MyBoxSetting#vMinBox
     * 
     */
    private Box vMinBox;
    /**
     * conteneur des de la combobox de la vieMax
     * 
     * @see MyBoxSetting#vMaxBox
     * 
     */
    private Box vMaxBox;
    /**
     * combo box contenant les valeurs du param�tre du jeu mortSolitude de la
     * cellule
     */
    private JComboBox<Integer> mortSCbox;
    /**
     * combo box contenant les valeurs du param�tre du jeu mortAsphyxie de la
     * cellule
     */
    private JComboBox<Integer> mortACbox;
    /**
     * combo box contenant les valeurs du param�tre du jeu vie minimum de la
     * cellule
     */
    private JComboBox<Integer> vMinCbox;
    /**
     * combo box contenant les valeurs du param�tre du jeu vie maximum
     */
    private JComboBox<Integer> vMaxCbox;
    /**
     * titre de la combo box mortSCbox
     * 
     * @see MyBoxSetting#mortSCbox
     */
    private JLabel mortSLabel;
    /**
     * titre de la combo box mortACbox
     * 
     * @see MyBoxSetting#mortACbox
     */
    private JLabel mortALabel;
    /**
     * titre de la combo box vMinBox
     * 
     * @see MyBoxSetting#vMinBox
     */
    private JLabel vMinLabel;
    /**
     * titre de la combo boxvMaxBox
     * 
     * @see MyBoxSetting#vMaxBox
     */
    private JLabel vMaxLabel;
    /**
     * titre de la zone des param�tres du jeu
     */
    private JLabel paramLabel;

    /**
     * Constructeur de la classe MyBoxSetting
     * 
     * @param axis
     *            nombre qui indique o� doit se placer le conteneur
     */
    public MyBoxSetting(int axis) {
        super(axis);
        init();
        this.add(paramLabel);
        this.add(mSBox);
        this.add(mABox);
        this.add(vMinBox);
        this.add(vMaxBox);
        this.add(Box.createRigidArea(new Dimension(0, 20)));
        this.setBorder(BorderFactory.createLineBorder(Color.red));
    }

    /**
     * methode qui initialise les composants du conteneur (de la classe
     * MyBoxSetting)
     */
    public void init() {
        mSBox = Box.createHorizontalBox();
        mABox = Box.createHorizontalBox();
        vMinBox = Box.createHorizontalBox();
        vMaxBox = Box.createHorizontalBox();

        mSBox.setAlignmentX(LEFT_ALIGNMENT);
        mABox.setAlignmentX(LEFT_ALIGNMENT);
        vMinBox.setAlignmentX(LEFT_ALIGNMENT);
        vMaxBox.setAlignmentX(LEFT_ALIGNMENT);

        mortSCbox = new JComboBox<>();
        mortACbox = new JComboBox<>();
        vMinCbox = new JComboBox<>();
        vMaxCbox = new JComboBox<>();

        paramLabel = new JLabel("Param�tres du jeu");

        mortSLabel = new JLabel("mort Solitude");
        mortALabel = new JLabel("mort Asphyxie");
        vMinLabel = new JLabel("vie Minimale");
        vMaxLabel = new JLabel("vie Maximale");

        mortSCbox.setMaximumSize(new Dimension(40, 20));
        mortSCbox.addItem(0);
        mortSCbox.addItem(1);

        mSBox.add(mortSLabel);
        mSBox.add(mortSCbox);

        mortACbox.setMaximumSize(new Dimension(40, 20));
        mortACbox.addItem(1);
        mortACbox.addItem(2);
        mortACbox.addItem(3);
        mortACbox.addItem(4);
        mortACbox.addItem(5);
        mortACbox.addItem(6);
        mortACbox.addItem(7);
        mortACbox.addItem(8);

        mABox.add(mortALabel);
        mABox.add(mortACbox);

        vMinCbox.setMaximumSize(new Dimension(40, 20));
        vMinCbox.addItem(1);
        vMinCbox.addItem(5);
        vMinCbox.addItem(10);
        vMinCbox.addItem(15);
        vMinCbox.addItem(20);

        vMinBox.add(vMinLabel);
        vMinBox.add(vMinCbox);

        vMaxCbox.setMaximumSize(new Dimension(40, 20));
        vMaxCbox.addItem(10);
        vMaxCbox.addItem(20);
        vMaxCbox.addItem(30);
        vMaxCbox.addItem(40);

        mSBox.add(mortSLabel);
        mSBox.add(mortSCbox);

        mABox.add(mortALabel);
        mABox.add(mortACbox);

        vMinBox.add(vMinLabel);
        vMinBox.add(vMinCbox);

        vMaxBox.add(vMaxLabel);
        vMaxBox.add(vMaxCbox);

    }

}
