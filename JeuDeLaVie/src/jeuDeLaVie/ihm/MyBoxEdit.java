package jeuDeLaVie.ihm;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import jeuDeLaVie.model.Plateau;

/**
 * @author fadwa
 * 
 *         conteneur des composant de la zone d'�dition du jeu
 *
 */
public class MyBoxEdit extends Box {

    /**
     * 
     */
    private static final long serialVersionUID = -4974798732005640643L;
    /**
     * champs pour �crire le nombre de lignes du tableau du jeu
     */
    private JTextField rowsGrid;
    /**
     * champs pour �crire le nombre de colonnes du tableau du jeu
     */
    private JTextField colsGrid;
    /**
     * bouton pour r�nisialiser le jeu et tuer toutes les cellules vivantes
     */
    private JButton renitButton;
    /**
     * bouton pour inisialiser le jeu
     */
    private JButton initAleaPlateauButton;

    /**
     * Retourne le bouton de r�nitialisation du jeu
     * 
     * @return le bouton de r�nisialisation de la box d'�dition
     */
    public JButton getRenitButton() {
        return renitButton;
    }

    /**
     * Met � jour le bouton de r�nisialisation du jeu
     * 
     * @param renitButton
     *            le nouveau bouton de r�nisialisation
     */
    public void setRenitButton(JButton renitButton) {
        this.renitButton = renitButton;
    }

    /**
     * Retourne le bouton d'initialisation du jeu
     * 
     * @return le bouton d'initialisation de la box d'�dition
     */
    public JButton getInitAleaPlateauButton() {
        return initAleaPlateauButton;
    }

    /**
     * Met � jour le bouton d'initialisation du jeu
     * 
     * @param initAleaPlateauButton
     *            le nouveau bouton d'initialisation
     */
    public void setInitAleaPlateauButton(JButton initAleaPlateauButton) {
        this.initAleaPlateauButton = initAleaPlateauButton;
    }

    /**
     * titre de la zone d'�dition du jeu
     */
    private JLabel editionLabel;

    /**
     * titre de la zone o� on change la taille
     */
    private JLabel taillePlateauLabel;
    /**
     * conteneur des textfields et label de changement de la taille du tableau
     * 
     * @see MyBoxEdit#taillePlateauLabel
     * @see MyBoxEdit#rowsGrid
     * @see MyBoxEdit#colsGrid
     */
    private Box boxTaille;
    /**
     * instance de la grille du jeu
     * 
     * @see MyBoxEdit#getPanelg()
     * @see MyBoxEdit#setPanelg(MyGrid)
     */
    private MyGrid panelg;

    /**
     * @author fadwa sous classe de la classe MyBoxEdit qui g�n�re une bo�te de
     *         dialogue personnalis�e
     *
     */
    class Dialog extends JDialog {
        /**
         * 
         */
        private static final long serialVersionUID = 6627012237919684970L;
        /**
         * panel de la bo�te de dialogue de comfirmation
         */
        protected JPanel panel;
        /**
         * bouton oui de la bo�te de dialogue
         */
        protected JButton yesButton;
        /**
         * bouton oui de la bo�te de dialogue
         */
        protected JButton noButton;
        /**
         * le texte de la bo�te de dialogue
         */
        public JLabel dialogLabel;

        /**
         * Retourne le texte de la boite de dialogue
         * 
         * @return texte de la boite de dialogue sous forme de chaine de
         *         caract�res
         */
        public String getDialogLabel() {
            return dialogLabel.getText();
        }

        /**
         * Met � jour le texte de la boite de dialogue
         * 
         * @param text
         *            le nouveau texte de la boite de dialogue
         */
        public void setDialogLabel(String text) {
            this.dialogLabel.setText(text);
        }

        /**
         * Ecouteur des composants des boutons de dialogues yesbutton et
         * nobutton
         * 
         * @see Dialog#yesButton
         * @see Dialog#noButton
         */
        ActionListener aL = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text = "Voulez vous R�nitialiser le tableau?";
                if (yesButton == e.getSource()) {
                    setEtat(false);
                    if (dialogLabel.getText() == text) {
                        actionRenit();
                    } else {
                        actionInit();
                    }

                } else if (noButton == e.getSource()) {
                    setEtat(false);
                }
            }
        };

        /**
         * methode pour changer r�nisialiser le jeu en fonction de la taille du
         * tableau du jeu
         */
        public void actionRenit() {
            MyGrid actionPanelGrid = MyPanel.getGridPanel();
            actionPanelGrid.setCols(Integer.parseInt(colsGrid.getText()));
            actionPanelGrid.setRows(Integer.parseInt(rowsGrid.getText()));
            // initialiser nouveau plateau
            MyPanel.getGridPanel().setPlateau(new Plateau(
                    actionPanelGrid.getRows() * actionPanelGrid.getCols()));
        }

        /**
         * methode pour initialiser al�atoirement le jeu
         */
        public void actionInit() {

        }

        /**
         * Met � jour l'�tat de visibilit� de la boite de dialogue
         * 
         * @param b
         *            nouveau �tat de visibilit� de la boite de dialogue
         */
        public void setEtat(boolean b) {
            this.setVisible(b);
        }

        /**
         * Constructeur de la classe personnalis�e Dialog
         */
        public Dialog() {
            panel = new JPanel();
            yesButton = new JButton("Oui");
            noButton = new JButton("Non");
            yesButton.addActionListener(aL);
            noButton.addActionListener(aL);
            dialogLabel = new JLabel();
            dialogLabel.setText(getDialogLabel());
            panel.add(dialogLabel, BorderLayout.NORTH);
            panel.add(yesButton, BorderLayout.SOUTH);
            panel.add(noButton, BorderLayout.SOUTH);
            this.add(panel);
            this.setTitle("Fen�tre de Confirmation");
            this.setVisible(true);
            this.setSize(new Dimension(300, 100));
        }
    }

    /**
     * action listener des boutons de la classe MyBoxEdit
     * 
     * @see MyBoxEdit#renitButton
     * @see MyBoxEdit#initAleaPlateauButton
     */
    ActionListener aL = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            Dialog dialog = new Dialog();
            if (renitButton == e.getSource()) {
                dialog.setDialogLabel("Voulez vous R�nitialiser le tableau?");
                dialog.setVisible(true);
            } else if (initAleaPlateauButton == e.getSource()) {
                dialog.setDialogLabel(
                        "Voulez vous Initialiser al�atoirement le tableau?");
                dialog.setVisible(true);
            }
        }
    };

    /**
     * Constructeur de la classe MyBoxEdit
     * 
     * @param axis
     *            nombre qui indique o� doit se placer le conteneur
     */
    public MyBoxEdit(int axis) {
        super(axis);
        init();
        this.add(editionLabel);
        this.add(boxTaille);
        this.add(renitButton);
        this.add(initAleaPlateauButton);
        this.setBorder(BorderFactory.createLineBorder(Color.red));
        this.add(Box.createRigidArea(new Dimension(0, 20)));

        if (MyPanel.getModeJeu() == "start") {
            renitButton.setEnabled(false);
            initAleaPlateauButton.setEnabled(false);
            MyGrid.setIsMouseListener(false);
            MyGrid.setColor(Color.WHITE);
        } else {
            renitButton.setEnabled(true);
            initAleaPlateauButton.setEnabled(true);
            MyGrid.setIsMouseListener(true);
            MyGrid.setColor(Color.black);
        }

    }

    /**
     * methode qui initialise les composants du conteneur (de la classe
     * MyBoxEdit)
     */
    public final void init() {
        boxTaille = new Box(BoxLayout.X_AXIS);

        editionLabel = new JLabel("Edition du jeu");
        taillePlateauLabel = new JLabel("Changer taille du plateau");
        initAleaPlateauButton = new JButton("Initialiser al�atoirement");
        initAleaPlateauButton.addActionListener(aL);

        rowsGrid = new JTextField();
        rowsGrid.setMaximumSize(new Dimension(20, 20));
        colsGrid = new JTextField();
        colsGrid.setMaximumSize(new Dimension(20, 20));

        renitButton = new JButton("Reniatialiser plateau");
        renitButton.addActionListener(aL);

        boxTaille.setAlignmentX(LEFT_ALIGNMENT);
        boxTaille.add(taillePlateauLabel);
        boxTaille.add(rowsGrid);
        boxTaille.add(colsGrid);
    }
    
    /**
     * Retourne le tableau de rectangles
     * 
     * @return une instance du tableau
     */
    public MyGrid getPanelg() {
        return panelg;
    }

    /**
     * Met � jour le tableau de rectangles
     * 
     * @param panelGrille
     *            le nouveau tableau de rectangles
     */
    public void setPanelg(MyGrid panelGrille) {
        this.panelg = panelGrille;
    }

    // test la classe
    public static void main(String[] args) {
        JFrame f = new JFrame();
        f.add(new MyBoxEdit(BoxLayout.Y_AXIS));
        f.setVisible(true);
    }

   
}
