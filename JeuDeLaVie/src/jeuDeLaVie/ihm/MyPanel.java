package jeuDeLaVie.ihm;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * @author fadwa
 * 
 *         Cette classe g�n�re le panel de la classe MyFrame ainsi que les
 *         diff�rents composants
 */
public class MyPanel extends JPanel {
    private static final long serialVersionUID = 1L;

    /**
     * Instance du tableau du jeu
     * 
     * @see MyPanel#MyPanel()
     * @see MyPanel#setGridPanel(MyGrid)
     * @see MyPanel#getGridPanel()
     */
    private static MyGrid gridPanel;
    /**
     * pausePlayButton bouton lancera et arr�tera le jeu de la vie sur le
     * plateau de jeu
     */
    private JButton pausePlayButton;
    /**
     * icone pause du bouton pausePlaybutton
     */
    private ImageIcon pauseIcon;
    /**
     * icone play du bouton pausePlaybutton
     */
    private ImageIcon startIcon;
    /**
     * bouton pour fermer l'application
     * 
     * @see MyPanel#initB()
     * @see MyPanel#bQuit
     */
    private JButton quitButton;

    /**
     * variable du mode du jeu
     * @see MyPanel#setModeJeu(String)
     * @see MyPanel#getModeJeu()
     */
    private static String modeJeu;

    /**
     * Ecouteur sur les actions faites sur les boutons pausePlayButton et
     * quitButton
     */
    private ActionListener aL = new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if (pausePlayButton == e.getSource()) {
                // si l'icone est sur start elle devient pause vice-versa
                if (pausePlayButton.getIcon() == startIcon) {
                    pausePlayButton.setIcon(pauseIcon);
                    setModeJeu("start");
                } else {
                    pausePlayButton.setIcon(startIcon);
                    setModeJeu("pause");
                }

            } else if (quitButton == e.getSource()) {
                System.exit(0);
            }
        }
    };
    /**
     * Conteneur box du bouton pausePlayButton
     * 
     * @see MyPanel#initPanelG()
     */
    private Box boxpp;

    /**
     * Conteneur box des composants de la partie gauche du panel
     * 
     * @see MyPanel#initPanelG()
     * @see MyBoxEdit
     * @see MyBoxSetting
     */
    private Box boxG;

    /**
     * Conteneur du tableau de la zone tampon
     * 
     * @see MyPanel#initPanelD()
     */
    private Box boxtampon;

    /**
     * Instance du petit tableau de zone tampon du jeu
     * 
     * @see MyPanel#initPanelD()
     * @see MyPanel#boxtampon
     */
    private MyGrid tamponGridPanel;
    /**
     * Conteneur du bouton charger
     * 
     * @see MyPanel#initPanelD()
     */

    private Box boxcharger;
    /**
     * Conteneur de la zone des composants du jeu pr�d�fini
     * 
     * @see MyPanel#initPanelD()
     */
    private Box boxjeuxP;
    /**
     * Combo box des mod�les de jeu
     * 
     * @see MyPanel#initPanelD()
     * @see MyPanel#boxjeuxP
     */
    private JComboBox<String> jeuxPCbox;
    /**
     * Titre pour le combo box jeuxPCbox
     * 
     * @see MyPanel#initPanelD()
     * @see MyPanel#jeuxPCbox
     */
    private JLabel jeuxPLabel;
    /**
     * Titre pour le combo box jeuxPCbox
     * 
     * @see MyPanel#initPanelD()
     * @see MyPanel#boxcharger
     */
    private JButton chargerButton;

    /**
     * Conteneur box des composants de la partie droite du panel
     * 
     * @see MyPanel#initPanelD()
     */
    private Box boxD;
    /**
     * Conteneur box du bouton quit
     * 
     * @see MyPanel#initB()
     */
    private Box bQuit;

    /**
     * Constructeur de la classe Panel
     */

    public MyPanel() {
        this.setBackground(Color.white);
        this.setLayout(new BorderLayout());
        initPanelG();
        gridPanel = new MyGrid(100, 100, 5);
        this.add(gridPanel, BorderLayout.CENTER);
        initPanelD();
        initB();
    }

    /**
     * m�thode qui initialise les composants de la partie gauche du panel
     */
    public void initPanelG() {

        boxpp = Box.createHorizontalBox();
        boxpp.setAlignmentX(LEFT_ALIGNMENT);

        pauseIcon = new ImageIcon(MyPanel.class.getResource("pause.png"));
        startIcon = new ImageIcon(MyPanel.class.getResource("start.png"));
        setModeJeu("pause");
        pausePlayButton = new JButton(startIcon);
        pausePlayButton.addActionListener(aL);
        boxpp.add(pausePlayButton);
        /////////////////////////////
        boxG = Box.createVerticalBox();

        boxG.add(new MyBoxEdit(BoxLayout.Y_AXIS));
        boxG.add(Box.createRigidArea(new Dimension(0, 20)));
        boxG.add(new MyBoxSetting(BoxLayout.Y_AXIS));
        boxG.add(Box.createRigidArea(new Dimension(0, 20)));
        boxG.add(boxpp);

        this.add(boxG, BorderLayout.WEST);
    }

    /**
     * m�thode qui initialise les composants de la partie droite du panel
     */
    public void initPanelD() {
        tamponGridPanel = new MyGrid(10, 10, 5);

        boxD = Box.createVerticalBox();

        boxjeuxP = Box.createHorizontalBox();
        boxtampon = Box.createHorizontalBox();
        boxcharger = Box.createHorizontalBox();

        chargerButton = new JButton("Charger");

        jeuxPCbox = new JComboBox<>();

        jeuxPLabel = new JLabel("Jeux pr�d�finis");

        boxtampon.setAlignmentX(TOP_ALIGNMENT);
        boxcharger.setAlignmentX(TOP_ALIGNMENT);
        boxD.setAlignmentX(TOP_ALIGNMENT);

        jeuxPCbox.setMaximumSize(new Dimension(40, 20));
        jeuxPCbox.addItem("a");
        jeuxPCbox.addItem("b");
        jeuxPCbox.addItem("c");

        boxjeuxP.add(jeuxPLabel);
        boxjeuxP.add(jeuxPCbox);

        boxcharger.add(chargerButton);
        boxtampon.add(tamponGridPanel);

        boxD.add(boxtampon);
        boxD.add(boxjeuxP);
        boxD.add(boxcharger);

        boxD.add(Box.createVerticalGlue());
        boxD.add(Box.createVerticalGlue());
        boxD.add(Box.createVerticalGlue());
        this.add(boxD, BorderLayout.EAST);
    }

    /**
     * m�thode qui initialise les composants du bas du panel
     */
    public void initB() {
        bQuit = Box.createHorizontalBox();

        quitButton = new JButton("Quitter");
        quitButton.setSize(40, 20);
        quitButton.addActionListener(aL);
        bQuit.add(Box.createHorizontalGlue());
        bQuit.add(quitButton);
        bQuit.setAlignmentX(RIGHT_ALIGNMENT);
        this.add(bQuit, BorderLayout.SOUTH);
    }

    // test la classe MyPanel
    public static void main(String[] args) {
        JFrame f = new JFrame();
        f.add(new MyPanel());
        f.setPreferredSize(new Dimension(1000, 600));
        f.pack();
        f.setVisible(true);
    }

    /**
     * Met � jour le tableau du jeu
     * 
     * @param g
     *            le nouveau tableau du jeu
     */
    public static void setGridPanel(MyGrid g) {
        gridPanel = g;
    }

    /**
     * Retourne le tableau du jeu
     * 
     * @return une instance du tableau du jeu
     */
    public static MyGrid getGridPanel() {
        return gridPanel;
    }

    /**
     * Retourn le mode du jeu
     * 
     * @return le mode de jeu sous forme d'une cha�ne de caract�res
     */
    public static String getModeJeu() {
        return modeJeu;
    }

    /**
     * Met � jour le mode du jeu
     * 
     * @param modeJeu
     *            le nouveau mode du jeu
     * 
     */
    public void setModeJeu(String modeJeu) {
        this.modeJeu = modeJeu;
    }

}
