package jeuDeLaVie.model;

public class Plateau {

    private int taillePlateau;
    private Cellule[][] tableau;
    private int time = 0;

    public Plateau(int n) {

        taillePlateau = n;
        tableau = new Cellule[taillePlateau][taillePlateau];

        for (int i = 0; i < taillePlateau; i++) {
            for (int j = 0; j < taillePlateau; j++) {
                tableau[i][j] = new Cellule(false);
            }
        }

        /* pour le for en dessous :
         * Algo � changer (ajout des cas) en fonction de tout 
         * les cas des voisins des cellules pour que �a marche :
         * (les coins de chaques cot�s +
         *  la premi�re ligne et la derni�re(sans les coins droits et gauches) +
         *  la premi�re et derni�re colonnes (sans les coins sup�rieurs et inf�rieurs))
         */
        
        for (int i = 0; i < taillePlateau; i++) {
            for (int j = 0; j < taillePlateau; j++) {
                if (i - 1 >= 0 && j - 1 >= 0 && i + 1 < taillePlateau
                        && j + 1 < taillePlateau) {
                    System.out.println("i  : "+i+" j  : "+j);
                    tableau[i][j].ajouteVoisin(tableau[i - 1][j - 1]);
                    tableau[i][j].ajouteVoisin(tableau[i][j - 1]);
                    tableau[i][j].ajouteVoisin(tableau[i + 1][j - 1]);
                    tableau[i][j].ajouteVoisin(tableau[i - 1][j]);
                    tableau[i][j].ajouteVoisin(tableau[i + 1][j]);
                    tableau[i][j].ajouteVoisin(tableau[i - 1][j + 1]);
                    tableau[i][j].ajouteVoisin(tableau[i][j + 1]);
                    tableau[i][j].ajouteVoisin(tableau[i + 1][j + 1]);
                }
            }
        }
    }

    public Cellule getTableau(int i , int j) {
        return tableau[i][j];
    }

    public void setCellule(int x, int y) throws Exception {
        if (x < 0 || y < 0 || x >= taillePlateau || y >= taillePlateau) {
            throw new Exception("Taille max");
        } else {
            tableau[x][y].setEtat(true);
        }
    }

    public void affiche() {
        System.out.println("");
        for (int i = 0; i < taillePlateau; i++) {
            for (int j = 0; j < taillePlateau; j++) {
                System.out.print(" | " + tableau[i][j]);
            }
            System.out.print(" | ");
            System.out.println("");
        }
    }

    public void update() {

    }
}
