package jeuDeLaVie.model;

import java.util.*;

public class Cellule {

    private boolean etat;
    private int mortSolitude = 1;
    private int mortAsphyxie = 4;
    private int vieMin = 3;
    private int vieMax = 3;
    
    public List<Cellule> voisins;

    public Cellule(boolean b) {
        this.voisins = new ArrayList<>();
        this.etat = b;
    }

    public void ajouteVoisin(Cellule c) {
        this.voisins.add(c);
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean b) {
        this.etat = b;
        for (Cellule c : this.voisins) {
            c.testVoisins();
        }
        this.testVoisins();
    }

    public int getMortSolitude() {
        return mortSolitude;
    }

    public void setMortSolitude(int mortSolitude) {
        this.mortSolitude = mortSolitude;
    }

    public int getMortAsphyxie() {
        return mortAsphyxie;
    }

    public void setMortAsphyxie(int mortAsphyxie) {
        this.mortAsphyxie = mortAsphyxie;
    }

    public int getVieMin() {
        return vieMin;
    }

    public void setVieMin(int vieMin) {
        this.vieMin = vieMin;
    }

    public int getVieMax() {
        return vieMax;
    }

    public void setVieMax(int vieMax) {
        this.vieMax = vieMax;
    }

    public void testVoisins() {
        int cptActif = 0;
        for (Cellule c : this.voisins) {
            if (c.getEtat()) {
                cptActif++;
            }
        }
        if (cptActif <= mortSolitude || mortAsphyxie >= 4) {
            etat = false;
        }
    }
}
